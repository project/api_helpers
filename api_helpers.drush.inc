<?php

/**
 * @file
 * Drush commands for the API helpers module.
 */

/**
 * Implements hook_drush_command().
 */
function api_helpers_drush_command() {
  return array(
    'api-helpers-quick-add-branch' => array(
      'aliases' => array('apiqab'),
      'description' => 'Quickly add or update an API branch and project',
      'arguments' => array(
        'project' => 'Project name',
        'branch' => 'Branch name',
      ),
      'options' => array(
        'project_type' => 'Project type (core or module)',
      ),
    ),
  );
}

/**
 * Adds or updates an API branch, and project if it doesn't already exist.
 */
function drush_api_helpers_quick_add_branch($project, $branch) {
  $project_type = drush_get_option('project_type');
  if (empty($project_type)) {
    $project_type = 'module';
  }

  // Directories.
  $curcwd = getcwd();
  $working_dir = variable_get('api_helpers_base_dir', '');
  $destination = $working_dir . '/' . $project . '-' . $branch;

  if (empty($working_dir)) {
    drush_log(dt('Please set the variable api_base_dir to the right location of your git clones'), 'error');
    return;
  }

  // If the directory exists, refresh.
  if (is_dir($destination)) {
    chdir($destination);
    $command = "git pull";
    drush_shell_exec($command);
    chdir($curcwd);
  }
  else {
    // Build request for package_handler_download_project.
    $request['name'] = $project;
    $request['full_project_path'] = $destination;
    $release['tag'] = $branch;

    // Clone from drupal.
    drush_include_engine('package_handler', 'git_drupalorg');
    package_handler_download_project($request, $release);
  }

  // If no info file exists, return.
  if (!file_exists($destination . '/' . $project . '.info')) {
    drush_log(dt('Info file not found (!dir).', array('!dir' => $destination . '/' . $project . '.info')), 'error');
  }
  else {
    // Parse module info file.
    $project_info = drupal_parse_info_file($destination . '/' . $project . '.info');
    $project_title = $project_info['name'];
    $core_compatibility = $project_info['core'];
    $branch_title = $project_title . ' (' . $branch . ')';

    return drush_api_ensure_branch($project, $project_title, $project_type, $branch, $branch_title, $destination, $core_compatibility, 604800);
  }
}
